# Jubilee - babySRL

Branch of the [Jubilee project](http://code.google.com/p/propbank/) for the BabySRL annotation.

## Prerequisites
Java 7 (1.7)

## Usage
First, make sure the folder containing the main `jubilee.jar` file contains the following folders:

* `system`
  * annotation properties + lists of Penn Treebank terminal/non-terminal node labels 
  * the BabySRL folder files can be found [here](https://s3.us-east-2.amazonaws.com/com.christos-c.share/jubilee-system.zip)
* `resource/annotation` 
  * initially empty
* `resource/frameset`
  * contains all Propbank `.xml` frame files
* `resource/task`
  * contains the task files in JSON format (see below)

Then, in the command line, navigate to the project's folder and run:

    java -jar jubilee.jar -u <USERNAME>

### Keyboard shortcuts
The **Help** menu (`F1`) will provide the same list of shortcuts

| Corpus File Actions |                  |
|---------------------|------------------|
| Open new file       | `cmd/ctrl` + `o` |
| Save current file   | `cmd/ctrl` + `s` |

| Navigation Actions  |                  |
|---------------------|------------------|
| Next annotation     | `.`              |
| Previous annotation | `,`              |
| Jump to annotation  | `cmd/ctrl` + `j` |
| Show incremental IDs| `cmd/ctrl` + `i` |

| Frameset Information|                  |
|---------------------|------------------|
| Next roleset        | `]`              |
| Previous roleset    | `[`              |
| See examples        | `cmd/ctrl` + `e` |

| Annotation/Editing  |                           |
|---------------------|---------------------------|
| Edit tree           | `cmd/ctrl` + `t`          |
| Add predicate       | `=`                       |
| Add Arg[0-5]        | `0-5`                     |
| Add ArgP-Gov        | `x`                       |
| Add ArgP-Obj        | `y`                       |
| Approve annotation  | `cmd/ctrl` + `shift` + `a`|

## Major changes from original Jubilee
* Using JSON format and storing the treebank internally (see below)
* Ability to delete entire annotation (if there was no real predicate) and creating a new one (for missing predicates)
* Ability to edit syntax trees (with bracket highlighting)
* Added context window that shows N previous/following utterances
* Added IAA calculator
* Ability to view predicate information (URL link to Propbank)
* Added bookmarking ability to allow annotators to save difficult annotation cases for future discussion

### JSON annotation file format
```json
{
   "annotations":[
      {
         "indexInContext":0,
         "pbInstance":"auto play play.01#1:1-ARG1 0:0-rel",
         "tree":"(S1(S(VP(VB play)(NP(NNS checkers)))(. .)))"
      },
      {
         "indexInContext":1,
         "pbInstance":"auto drum drum.01#0:1-ARG0 1:0-rel",
         "tree":"(S1(S(NP(JJ big))(VP(VB drum))(. .)))"
      }
   ],
   "contexts":[
      "0 *CHI:\tplay checkers .",
      "1 *CHI:\tbig drum .",
      "2 *MOT:\tbig drum ?"
   ]
}
```