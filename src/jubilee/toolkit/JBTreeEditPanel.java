package jubilee.toolkit;

import jubilee.datastructure.JBDataStructure;
import jubilee.util.StringManager;

import javax.swing.*;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.DefaultHighlighter;
import javax.swing.text.Highlighter;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;

/**
 * A panel that allows the editing of the syntactic tree of an utterance
 *
 * @author Christos Christodoulopoulos
 */
public class JBTreeEditPanel extends JFrame implements ActionListener {
    private final JTextArea textArea;
    private JButton buttonApply, buttonCancel;
    private JBDataStructure annotation;
    private JTextField fieldWordsNum, fieldWordPos;

    JBToolkit parent;

    public JBTreeEditPanel(JBToolkit parent, JBDataStructure annotation) {
        super("Syntax tree edit");

        this.annotation = annotation;
        this.parent = parent;

        textArea = new JTextArea(annotation.getTbTree().toTextTree());
        textArea.setMargin(new Insets(10, 10, 10, 10));
        textArea.addCaretListener(new CaretListenerLabel(textArea));

        buttonApply = new JButton("Apply");
        buttonApply.addActionListener(this);
        buttonCancel = new JButton("Cancel");
        buttonCancel.addActionListener(this);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.X_AXIS));
        buttonPanel.add(buttonApply);
        buttonPanel.add(Box.createRigidArea(new Dimension(10, 0)));
        buttonPanel.add(buttonCancel);

        String sentStr = annotation.getTbTree().getSentence();
        String sentLabelStr = "<html>Sentence: <i>" + sentStr + "</i></html>";
        JLabel sentLabel = new JLabel(sentLabelStr);

        fieldWordsNum = new JTextField();
        fieldWordsNum.setMaximumSize(new Dimension(35, 20));
        fieldWordsNum.setPreferredSize(new Dimension(35, 20));
        fieldWordPos = new JTextField();
        fieldWordPos.setMaximumSize(new Dimension(35, 20));
        fieldWordPos.setPreferredSize(new Dimension(35, 20));
        JPanel addArgsPanel = new JPanel();
        addArgsPanel.setLayout(new BoxLayout(addArgsPanel, BoxLayout.X_AXIS));
        addArgsPanel.add(new JLabel("#new words inserted"));
        addArgsPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        addArgsPanel.add(fieldWordsNum);
        addArgsPanel.add(Box.createHorizontalGlue());
        addArgsPanel.add(new JLabel("#words before new word"));
        addArgsPanel.add(Box.createRigidArea(new Dimension(5, 0)));
        addArgsPanel.add(fieldWordPos);
        addArgsPanel.setBorder(BorderFactory.createTitledBorder(
                BorderFactory.createLineBorder(Color.BLACK), "Reposition SRL labels"));
        addArgsPanel.setMaximumSize(new Dimension(390, 50));

        // Set layout and add components
        JPanel leftPanel = new JPanel();
        leftPanel.setLayout(new BoxLayout(leftPanel, BoxLayout.Y_AXIS));

        JScrollPane textScrollPane = new JScrollPane(textArea);
        textScrollPane.setMaximumSize(new Dimension(390, 290));
        textScrollPane.setPreferredSize(new Dimension(390, 290));
        leftPanel.add(textScrollPane);
        sentLabel.setAlignmentX(Component.CENTER_ALIGNMENT);
        leftPanel.add(sentLabel);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 30)));
        leftPanel.add(addArgsPanel);
        buttonPanel.setAlignmentY(Component.BOTTOM_ALIGNMENT);
        leftPanel.add(Box.createRigidArea(new Dimension(0, 20)));
        leftPanel.add(buttonPanel);
        leftPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));
        leftPanel.setMaximumSize(new Dimension(400, 480));

        JPanel rightPanel = new JPanel();
        rightPanel.setLayout(new BoxLayout(rightPanel, BoxLayout.Y_AXIS));
        Dimension referenceDimension = new Dimension(290, 190);

        JTextArea referenceNonTerminalsTextArea = new JTextArea(parent.getNonTerminalsList());
        referenceNonTerminalsTextArea.setEditable(false);
        JScrollPane referenceNonTerminalsScrollPane = new JScrollPane(referenceNonTerminalsTextArea);
        referenceNonTerminalsScrollPane.setMinimumSize(referenceDimension);
        referenceNonTerminalsScrollPane.setPreferredSize(referenceDimension);

        JTextArea referenceTerminalsArea = new JTextArea(parent.getTerminalsList());
        referenceTerminalsArea.setEditable(false);
        JScrollPane referenceTerminalsScrollPane = new JScrollPane(referenceTerminalsArea);
        referenceTerminalsScrollPane.setMinimumSize(referenceDimension);
        referenceTerminalsScrollPane.setPreferredSize(referenceDimension);

        rightPanel.add(new JLabel("Reference lists:"));
        rightPanel.add(new JLabel("Non-Terminal labels"));
        rightPanel.add(referenceNonTerminalsScrollPane);
        rightPanel.add(new JLabel("Terminal labels"));
        rightPanel.add(referenceTerminalsScrollPane);
        rightPanel.setBorder(BorderFactory.createEmptyBorder(5, 5, 5, 5));

        setLayout(new BoxLayout(getContentPane(), BoxLayout.X_AXIS));
        leftPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        rightPanel.setAlignmentY(Component.TOP_ALIGNMENT);
        add(leftPanel);
        add(rightPanel);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                dispose();
            }
        });

        setBounds(20, 20, 700, 480);
        setVisible(true);
    }

    @Override
    public void actionPerformed(ActionEvent evt) {
        if (evt.getSource() == buttonCancel)
            dispose();
        else if (evt.getSource() == buttonApply) {
            int newWords = 0, wordPos = 0;
            // Check if the user added #new words/word position
            if (!fieldWordPos.getText().isEmpty() || !fieldWordsNum.getText().isEmpty()) {
                try {
                    newWords = Integer.parseInt(fieldWordsNum.getText());
                    wordPos = Integer.parseInt(fieldWordPos.getText());
                } catch (NumberFormatException e) {
                    JOptionPane.showMessageDialog(null, "Please type numbers to both fields",
                            "Error", JOptionPane.ERROR_MESSAGE);
                    return;
                }
            }
            try {
                verifyBrackets(textArea.getText());
                if (newWords != 0) annotation.setTree(textArea.getText(), newWords, wordPos);
                else annotation.setTree(textArea.getText());
                parent.updateAll();
                dispose();
            } catch (Exception ex) {
                JOptionPane.showMessageDialog(null, "There was an error: \n" + ex.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void verifyBrackets(String text) throws Exception {
        int openBrackets = 0, closeBrackets = 0;
        for (char c : text.toCharArray()) {
            if (c == '(') openBrackets++;
            else if (c == ')') closeBrackets++;
        }
        if (openBrackets != closeBrackets) throw new Exception("Brackets mismatch");
    }

    protected class CaretListenerLabel extends JLabel implements CaretListener {
        private final Highlighter highlighter;
        private final JTextArea textArea;
        private final DefaultHighlighter.DefaultHighlightPainter p;

        public CaretListenerLabel(JTextArea textArea) {
            this.textArea = textArea;
            highlighter = textArea.getHighlighter();
            p = new DefaultHighlighter.DefaultHighlightPainter(Color.LIGHT_GRAY);
        }

        public void caretUpdate(CaretEvent e) {
            highlighter.removeAllHighlights();
            highlightMatchingBracket(e.getDot());
        }

        protected void highlightMatchingBracket(final int dot) {
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    String text = textArea.getText();
                    if (dot > 0 && (text.charAt(dot-1) == '(' || text.charAt(dot-1) == ')'))
                        highlight(dot-1);
                }
            });
        }

        private void highlight(int dot) {
            String text = textArea.getText();
            int matchingBracketInd = StringManager.findMatchingBracket(text, dot);
            try {
                if (matchingBracketInd == -1)
                    highlighter.addHighlight(dot, dot + 1, new DefaultHighlighter.DefaultHighlightPainter(Color.RED));
                else {
                    highlighter.addHighlight(matchingBracketInd, matchingBracketInd + 1, p);
                    highlighter.addHighlight(dot, dot + 1, p);
                }
            } catch (BadLocationException e) {
                e.printStackTrace();
            }
        }
    }
}
