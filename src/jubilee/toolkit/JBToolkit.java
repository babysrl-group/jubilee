/**
* Copyright (c) 2007, Regents of the University of Colorado
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the University of Colorado at Boulder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
package jubilee.toolkit;

import jubilee.agreement.AgreementCalculator;
import jubilee.awt.JDCBookmarksFrame;
import jubilee.awt.JDCFileDialog;
import jubilee.datastructure.JBCorpus;
import jubilee.datastructure.JBDataStructure;
import jubilee.util.DataManager;
import jubilee.util.StringManager;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

@SuppressWarnings("serial")
public class JBToolkit extends JFrame implements ActionListener, ItemListener, ListSelectionListener {
    public final static String EDITED = "[EDITED] ";
    private final static String GOLD_MISSING = "MISSING";
    private final static String WHITE_STAR = "\u2606";
    private final static String BLACK_STAR = "\u2605";
    private String str_frameTitle;
	private String str_userID;
    // dataset[0] = resource, dataset[1] = directory-paths
    private HashMap<String, String> str_dataset;
    // path of annotation file
    private String str_annFile;

    // Treeview: top-pane
	private JButton bt_prev, bt_next, buttonTreeEdit, buttonRemoveAnnotation, buttonAddAnnotation, buttonBookmark;
	private JComboBox<String> comboJump;
	private JTextField tf_annotator;

	private JList<String> ls_gold;
	private DefaultListModel<String> lm_gold;
	private JBTreePanel tv_tree;

	private JBFramesetPanel framesetPanel;
    private JBContextPanel sentencePane;
	private JBArgPanel argPanel;
	private JBMenuBar mbar;
    private JBCorpus corpus;
	private JBCorpus[] moreCorpora;

	int i_currSetting = 0;		// current project setting (i.e. english.sample.path)
	private int i_maxAnn;

    // Reference lists (shown during tree editing)
    private ArrayList<String[]> terminalsList;
    private ArrayList<String[]> nonTerminalsList;

	static public String s_sysDir   = null;

    private static JBToolkit instance;
    private JCheckBox checkboxApprove;

    public JBToolkit(final String title, String sysDir, String userID, final int maxAnn) {
		super(title);
		str_frameTitle = title;
		s_sysDir = sysDir;
		str_userID = userID;
		i_maxAnn = maxAnn;

		Container cp = getContentPane();
		initComponents();
		initBounds(cp);

        showOpenDialog();

		setDefaultCloseOperation(WindowConstants.DO_NOTHING_ON_CLOSE);
        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosing(WindowEvent e) {
                if (!getTitle().startsWith(EDITED)) {
					setVisible(false);
					showOpenDialog();
				}
                else {
					int confirm = JOptionPane.showOptionDialog(null, "Save to file and exit?",
							"Exit Confirmation", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE, null, null, null);
					if (confirm == 0) {
						menuFileSave();
						setVisible(false);
						showOpenDialog();
					}
				}
            }
        });
        instance = this;
	}

    public static JBToolkit getInstance() {
        // This shouldn't be called before the creation of the main interface
        if (instance == null) {
            System.err.println("Someone is requesting an instance of the main interface prior to initialization");
            System.exit(-1);
        }
        return instance;
    }

    boolean isGold()
	{
		return str_userID.equalsIgnoreCase(DataManager.GOLD_ID);
	}

	String getUserID()
	{
		return str_userID;
	}

    private void showOpenDialog() {
        new JBOpenDialog(this, false, i_maxAnn);
        setVisible(true);
    }

    /**
     * Reads and stores reference material (related to the annotation guidelines) for later use
     */
    void readReferenceMaterial() {
        terminalsList = DataManager.getContents("terminals.txt");
        nonTerminalsList = DataManager.getContents("non-terminals.txt");
    }

	// -------------------- initialize components --------------------

	private void initComponents() {
		bt_prev = new JButton("Prev");
		bt_prev.addActionListener(this);

		bt_next = new JButton("Next");
		bt_next.addActionListener(this);

        checkboxApprove = new JCheckBox("Approve");
        checkboxApprove.addActionListener(this);
        checkboxApprove.setToolTipText("Check this box to approve the annotation \neven if no changes were made");

        buttonTreeEdit = new JButton("Edit Tree");
        buttonTreeEdit.addActionListener(this);

        buttonRemoveAnnotation = new JButton("[-] Remove Annotation");
        buttonAddAnnotation = new JButton("[+] Add Annotation");
        buttonRemoveAnnotation.addActionListener(this);
        buttonAddAnnotation.addActionListener(this);

		comboJump = new JComboBox<>();
		comboJump.setMaximumRowCount(20);
		comboJump.addItemListener(this);
		comboJump.setFocusable(false);

        buttonBookmark = new JButton(WHITE_STAR);
        buttonBookmark.setPreferredSize(new Dimension(35, 10));
        buttonBookmark.setMargin(new Insets(1,1,1,1));
        buttonBookmark.addActionListener(this);
        buttonBookmark.setFocusable(false);

		tf_annotator = new JTextField(str_userID);
		tf_annotator.setEditable(false);
		tf_annotator.setFocusable(false);

		tv_tree = new JBTreePanel();

		framesetPanel = new JBFramesetPanel();
		framesetPanel.setBorder(new TitledBorder("Frameset View"));

		argPanel = new JBArgPanel(tv_tree, this);
		argPanel.setBorder(new TitledBorder("Argument View"));

		mbar = new JBMenuBar(this);
		setJMenuBar(mbar);
	}

	private JPanel getTreePanel() {
		JPanel treePanel = new JPanel();
		treePanel.setLayout(new BorderLayout());

		// top of the treeview
		JPanel top = new JPanel();
		top.setLayout(new BorderLayout());

		JPanel top1 = new JPanel();
		top1.setLayout(new GridBagLayout());
        GridBagConstraints c = new GridBagConstraints();
        c.fill = GridBagConstraints.HORIZONTAL;
        c.gridx = 0;
        c.gridy = 0;
        c.weightx = .5;
        top1.add(bt_prev, c);

        c.gridx = 1;
        c.weightx = .5;
		top1.add(bt_next, c);

        c.gridx = 2;
        top1.add(checkboxApprove);

        c.gridx = 3;
        c.weightx = .7;
        c.fill = GridBagConstraints.BOTH;
        c.insets = new Insets(0,5,0,5);
        top1.add(tf_annotator, c);

        c.gridx = 4;
        c.weightx = .5;
        c.insets = new Insets(0,0,0,0);
        top1.add(comboJump, c);

        c.gridx = 5;
        c.weightx = 0;
        top1.add(buttonBookmark, c);

		top.add(top1, BorderLayout.NORTH);

		if (isGold()) {
            lm_gold = new DefaultListModel<>();
			ls_gold = new JList<>(lm_gold);

			ls_gold.setFont(new Font("Courier", Font.PLAIN, 11));
			ls_gold.addListSelectionListener(this);
			ls_gold.setVisibleRowCount(4);
			ls_gold.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
			top.add(new JScrollPane(ls_gold), BorderLayout.SOUTH);
		}

		// bottom of the treeview
        sentencePane = new JBContextPanel(this);
		sentencePane.setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.add(sentencePane, BorderLayout.NORTH);

        JPanel treeEditPanel = new JPanel();
        treeEditPanel.setLayout(new GridLayout(1, 3, 50, 0));
		treeEditPanel.add(buttonAddAnnotation);
		treeEditPanel.add(buttonRemoveAnnotation);
		treeEditPanel.add(buttonTreeEdit);

        bottomPanel.add(treeEditPanel, BorderLayout.SOUTH);

		treePanel.add(top, BorderLayout.NORTH);
		treePanel.add(tv_tree, BorderLayout.CENTER);
		treePanel.add(bottomPanel, BorderLayout.SOUTH);
		treePanel.setBorder(new TitledBorder("Treebank View"));

		return treePanel;
	}

	private void initBounds(Container cp) {
		Dimension screenSize = Toolkit.getDefaultToolkit().getScreenSize();
		int fr_wt = (int)screenSize.getWidth() - 50;
		int fr_ht = (int)screenSize.getHeight() - 50;

		JSplitPane sp_right = new JSplitPane(JSplitPane.VERTICAL_SPLIT, framesetPanel, argPanel);
		JSplitPane sp_main = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, getTreePanel(), sp_right);

		sp_main.setDividerLocation((int)(fr_wt*0.65));
		sp_right.setDividerLocation((int)(fr_ht*0.5));

		cp.setLayout(new BorderLayout());
		cp.add(sp_main, BorderLayout.CENTER);
		setBounds(0, 0, fr_wt, fr_ht);
	}

	// called from JBOpenDialog
	void initProperties(String corpusStr, HashMap<String, String> dataset) {
		ArrayList<String[]> argTag = DataManager.getContents(corpusStr + DataManager.ARGS_FILE_EXT);

		str_dataset = dataset;
		mbar.setMenuArgTag(argTag);
		argPanel.updateArgButtons(argTag);
		framesetPanel.setProperties(str_dataset.get(DataManager.FRAMESET));
	}

	// called from JBOpenDialog
	void openFile(String[] filename, boolean isNewTask) throws Exception {
		str_annFile = str_dataset.get(DataManager.ANNOTATION) + File.separator + filename[0] + "." + str_userID;
		setTitle(str_frameTitle + " - " + filename[0]);

        if (isNewTask) {
            String taskFile = str_dataset.get(DataManager.TASK) + File.separator + filename[0];
            corpus = new JBCorpus(taskFile, str_userID);
        }
        else
            corpus = new JBCorpus(str_annFile, str_userID);

        if (isGold()) {
            moreCorpora = new JBCorpus[filename.length-1];
            for (int i=1; i<filename.length; i++) {
                filename[i] = str_dataset.get(DataManager.ANNOTATION) + File.separator + filename[i];
                String annotator = filename[i].substring(filename[i].lastIndexOf('.')+1);
                moreCorpora[i-1] = new JBCorpus(filename[i], annotator);
            }
            if (isNewTask) {
                corpus = new JBCorpus(moreCorpora);
            }
            AgreementCalculator.corpusAgreement(moreCorpora[0], moreCorpora[1]);
        }

		framesetPanel.setCorpus(corpus);

		// Create the context window
		buildContextWindow();

		buildComboBox();
        // Go to the last sentence that the user annotated
		comboJump.setSelectedIndex(corpus.getLastEditIndex());
	}

	// ---------------------- Listener/Adpater Start ----------------------

	public void actionPerformed(ActionEvent e) {
		if (e.getSource() == mbar.fileOpen)				menuFileOpen();
		else if (e.getSource() == mbar.fileSave)		menuFileSave();
		else if (e.getSource() == mbar.fileSaveAs)		menuFileSaveAs();
		else if (e.getSource() == mbar.fileQuit)		System.exit(0);
		else if (e.getSource() == mbar.shownIncrementalIDs)		toggleShowIDs();
		else if (e.getSource() == mbar.showBookmarks)		showBookmarks();
        else if (e.getSource() == mbar.approveChanges)	checkboxApprove.setSelected(mbar.approveChanges.isSelected());
        else if (e.getSource() == mbar.tbPrev || e.getSource() == bt_prev)	actionBtPrev();
		else if (e.getSource() == mbar.tbNext || e.getSource() == bt_next)	actionBtNext();
		else if (e.getSource() == mbar.tbJump)			menuTbJump();
		else if (e.getSource() == mbar.tbView || e.getSource() == buttonTreeEdit) actionButtonTreeEdit();
		else if (e.getSource() == mbar.fsPrev)					framesetPanel.prevRoleset();
		else if (e.getSource() == mbar.fsNext)					framesetPanel.nextRoleset();
		else if (e.getSource() == mbar.fsViewExample)			framesetPanel.showExample();
		else if (e.getSource() == mbar.fsViewArgument)			tv_tree.viewArgument();
		else if (e.getSource() == mbar.fsViewRolesetComment)	framesetPanel.viewRolesetComment();
		else if (e.getSource() == mbar.argErase)		argPanel.updateArg(e.getActionCommand());
		else if (e.getSource() == mbar.helpShortcuts)   menuHelpShortcuts();
		else if (e.getSource() == mbar.helpAbout)		menuHelpAbout();
        else if (e.getSource() == buttonRemoveAnnotation)       actionButtonRemoveAnnotation();
        else if (e.getSource() == buttonAddAnnotation)       	actionButtonAddAnnotation();
        else if (e.getSource() == buttonBookmark)       	actionButtonBookmark();
        menuArgumentArg(e);
        menuArgumentFunc(e);
	}

	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == comboJump && comboJump.getSelectedIndex() >= 0) {
			corpus.setCurrentAnnotationIndex(comboJump.getSelectedIndex(), checkboxApprove.isSelected());
            checkboxApprove.setSelected(false);
            corpus.backup(corpus.getCurrentAnnotation());
            updateAll();
            if (isGold()) {
                updateGoldList();
                updateGoldTopList();
            }
        }
	}

	public void valueChanged(ListSelectionEvent e) {
        // Used to accept one of the annotations as gold
        if (e.getSource() == ls_gold && ls_gold.getSelectedIndex() > 0) {
            if (lm_gold.get(ls_gold.getSelectedIndex()).equals(GOLD_MISSING)) return;
            JBDataStructure alignedAnnotation = findAlignedAnnotation(moreCorpora[ls_gold.getSelectedIndex() - 1],
                    corpus.getCurrentAnnotation());
            corpus.copyCurrent(alignedAnnotation);
            corpus.getCurrentAnnotation().setAnnotator(DataManager.GOLD_ID);
            corpus.getCurrentAnnotation().hasChanged(true);
            updateAll();
            if (isGold()) {
                updateGoldList();
                updateGoldTopList();
            }
        }
    }

	void updateAll() {
        JBDataStructure annotation = corpus.getCurrentAnnotation();
        tv_tree.setAnnotation(annotation);
		framesetPanel.updateFrameset(annotation.getType(), annotation.getRoleset());
        String annotator = annotation.getAnnotator();
        tf_annotator.setText(annotator);
        switch (annotator) {
            case "auto":
                tf_annotator.setBackground(Color.RED.brighter());
                break;
            case "babySRL":
                tf_annotator.setBackground(Color.GREEN);
                break;
            case "gold":
                tf_annotator.setBackground(Color.YELLOW);
                break;
            default:
                tf_annotator.setBackground(Color.LIGHT_GRAY);
                break;
        }

		// Update the context window
		int indexInContext = corpus.getCurrentAnnotation().getIndexInContext();
        sentencePane.setPosition(indexInContext);

        if (corpus.getCurrentAnnotationIndex() == corpus.getSize() - 1) {
            bt_next.setEnabled(false);
            if (!corpus.isAnnotationFinished()) {
                JOptionPane.showMessageDialog(null, "Congratulations!", "End of File", JOptionPane.INFORMATION_MESSAGE);
                corpus.setAnnotationFinished(true);
            }
        }
        else bt_next.setEnabled(true);
        if (corpus.getCurrentAnnotationIndex() == 0)
            bt_prev.setEnabled(false);
        else bt_prev.setEnabled(true);

        if (corpus.isBookmarked()) buttonBookmark.setText(BLACK_STAR);
        else buttonBookmark.setText(WHITE_STAR);
	}

    private void updateGoldList() {
        lm_gold.removeAllElements();
        int maxLength = maxAnnotationLength();
        JBDataStructure annotation = corpus.getCurrentAnnotation();
        lm_gold.addElement(StringManager.addIndent("GOLD", 12) +
                StringManager.addIndent(annotation.getPropbankTree(), maxLength + 3) +
                annotation.getTbTree().toTextTreeCompact());
        for (JBCorpus otherCorpus : moreCorpora) {
            JBDataStructure otherAnnotation = findAlignedAnnotation(otherCorpus, annotation);
            // Check for deleted annotations
            if (otherAnnotation == null) {
                lm_gold.addElement(GOLD_MISSING);
            }
            else {
                lm_gold.addElement(StringManager.addIndent(otherAnnotation.getAnnotator(), 12) +
                        StringManager.addIndent(otherAnnotation.getPropbankTree(), maxLength + 3) +
                        otherAnnotation.getTbTree().toTextTreeCompact());
            }
        }
        ls_gold.invalidate();
    }

    void updateGoldTopList() {
        int maxLength = maxAnnotationLength();
        JBDataStructure annotation = corpus.getCurrentAnnotation();
        lm_gold.setElementAt(StringManager.addIndent("GOLD", 12) +
                StringManager.addIndent(annotation.getPropbankTree(), maxLength + 3) +
                annotation.getTbTree().toTextTreeCompact(), 0);
        ls_gold.invalidate();
    }

    private JBDataStructure findAlignedAnnotation(JBCorpus otherCorpus, JBDataStructure annotation) {
        List<JBDataStructure> otherAnnotations = otherCorpus.getAnnotationsFromContext(annotation.getIndexInContext());
        if (otherAnnotations == null) return null;
        for (JBDataStructure otherAnnotation : otherAnnotations) {
			int indexDiff = Math.abs(otherAnnotation.getPredicateIndex() - annotation.getPredicateIndex());
			// Allow for +/- 2 difference in indices due to the insertion of empty nodes
			// (and other noise from the CHAT version of the data
            if (indexDiff < 3 && otherAnnotation.getType().equals(annotation.getType()))
                return otherAnnotation;
        }
        return null;
    }

    private int maxAnnotationLength() {
        int max = 0;
        for (JBCorpus otherCorpus : moreCorpora) {
            JBDataStructure otherAnnotation = findAlignedAnnotation(otherCorpus, corpus.getCurrentAnnotation());
            if (otherAnnotation == null) continue;
            int length = otherAnnotation.getPropbankTree().length();
            if (length > max) max = length;
        }
        return max;
    }

    private void actionButtonBookmark() {
        // If there is a bookmark, remove it
        if (buttonBookmark.getText().equals(BLACK_STAR)) {
            corpus.removeBookmark();
            buttonBookmark.setText(WHITE_STAR);
        }
        else {
            String response = JOptionPane.showInputDialog(null, "Enter a brief message for the bookmark", "Add bookmark",
                    JOptionPane.QUESTION_MESSAGE);
            if (response != null) {
                corpus.addBookmark(response);
                buttonBookmark.setText(BLACK_STAR);
            }
        }
        // Mark that the content has changed
        if (!getTitle().startsWith(EDITED))
            setTitle(JBToolkit.EDITED + getTitle());
    }

    private void actionButtonTreeEdit() {
        new JBTreeEditPanel(this, corpus.getCurrentAnnotation());
    }

    private void actionButtonRemoveAnnotation() {
        int response = JOptionPane.showConfirmDialog(null, "Do you really want to remove this annotation?\n" +
                        "THIS ACTION IS NOT REVERSIBLE!",
                "Warning", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        boolean delete = isYes(response);

        if (delete) {
            int index = corpus.getCurrentAnnotationIndex();
            corpus.removeCurrentAnnotation();

            // Rebuild the context window (in case we have removed all annotations for this utterance)
            buildContextWindow();

            // Rebuild the combo box
            buildComboBox();

            // Now move to the next annotation
            if (index < corpus.getSize()) comboJump.setSelectedIndex(index);
            else comboJump.setSelectedIndex(index - 1);

            // Finally mark that the corpus has changed
            if (!getTitle().startsWith(EDITED))
                setTitle(EDITED + getTitle());
        }
    }

    public static boolean isYes(int response) {
        boolean answer = false;
        if (response == JOptionPane.NO_OPTION) answer = false;
        else if (response == JOptionPane.YES_OPTION) answer = true;
        else if (response == JOptionPane.CLOSED_OPTION) answer = false;
        return answer;
    }

    private void actionButtonAddAnnotation() {
		String response = JOptionPane.showInputDialog(null, "This will add a new annotation by copying " +
						"the existing one.\nPlease enter the predicate lemma of the new annotation\n\n" +
						"If this is a preposition please add \"-p\" (e.g. on-p).", "Add annotation",
				JOptionPane.QUESTION_MESSAGE);

		if (response != null) {
			// Add the new annotation
            try {
                corpus.addAnnotation(str_userID, response);
            } catch (Exception e) {
                JOptionPane.showMessageDialog(null, "There was an error: \n" + e.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);
            }

            // Rebuild the combo box
			buildComboBox();

			// Now move to the next annotation
			comboJump.setSelectedIndex(corpus.getCurrentAnnotationIndex());

            // Finally mark that the corpus has changed
            if (!getTitle().startsWith(EDITED))
                setTitle(EDITED + getTitle());
		}
	}

    private void buildContextWindow() {
        sentencePane.buildContextList(corpus);
    }

    private void buildComboBox() {
        boolean showIDs = mbar.shownIncrementalIDs.getState();
        comboJump.removeAllItems();
        for (int i = 0; i < corpus.getSize(); i++) {
            JBDataStructure annotation = corpus.getAnnotation(i);
            String contextId = corpus.getContext(annotation.getIndexInContext());
            int annotationPosition = corpus.getAnnotationPosition(annotation);
            int totalSentAnnotations = corpus.getAnnotationsFromContext(annotation.getIndexInContext()).size();
            String predicate = annotation.getRoleset().split("\\.")[0];
            String item = contextId.substring(0, contextId.indexOf(":")) + " - " + predicate;
            item += " [" + annotationPosition + "/" + totalSentAnnotations + "]";
            if (showIDs) item = "(" + i + ") " + item;
            comboJump.insertItemAt(item, i);
        }
    }

	// ---------------------- Menu-File Action ----------------------

	private void menuFileOpen()
	{
		new JBOpenDialog(this, true, i_maxAnn);
	}

	private void menuFileSave()
	{
		menuFileSave(str_annFile);
	}

	private void menuFileSave(String filename) {
        try {
            corpus.saveCorpus(filename);
            String title = getTitle();
            if (title.startsWith(EDITED))
                setTitle(title.replace(EDITED, ""));
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "There was an error: \n" + e.getMessage(), "Error",
                    JOptionPane.ERROR_MESSAGE);
        }
    }

	// str_annFile stays
	private void menuFileSaveAs() {
		JDCFileDialog fd_load = new JDCFileDialog(this);
		String filename = fd_load.save();

		if (filename != null && corpus != null)
			menuFileSave(filename);
	}

    // -------------------- Menu-View Action --------------------

    private void toggleShowIDs() {
        buildComboBox();
        comboJump.setSelectedIndex(corpus.getLastEditIndex());
    }

    private void showBookmarks() {
        new JDCBookmarksFrame(this, corpus.getBookmarks());
    }

    // ------------------ Menu-Treebank Action ------------------

	private void actionBtPrev() {
        if (isGold())
            corpus.getCurrentAnnotation().setAnnotator(DataManager.GOLD_ID);
		for (int i=corpus.getCurrentAnnotationIndex(); i>0; i--) {
			corpus.setCurrentAnnotationIndex(i - 1, checkboxApprove.isSelected());
            checkboxApprove.setSelected(false);
			if (!isGold() || !isGoldSame())	break;
		}
		comboJump.setSelectedIndex(corpus.getCurrentAnnotationIndex());
	}

	private void actionBtNext() {
        if (isGold())
            corpus.getCurrentAnnotation().setAnnotator(DataManager.GOLD_ID);
		for (int i=corpus.getCurrentAnnotationIndex(); i<corpus.getSize()-1; i++) {
			corpus.setCurrentAnnotationIndex(i + 1, checkboxApprove.isSelected());
            checkboxApprove.setSelected(false);
			if (!isGold() || !isGoldSame())	break;
		}
		comboJump.setSelectedIndex(corpus.getCurrentAnnotationIndex());
    }

	private boolean isGoldSame() {
		String tmpPBT = corpus.getCurrentAnnotation().getTbTree().toPropbank();
        String tmpT = corpus.getCurrentAnnotation().getTbTree().toTextTreeCompact();
        String tmpRS = corpus.getCurrentAnnotation().getRoleset();

        for (JBCorpus otherCorpus : moreCorpora) {
            JBDataStructure otherAnnotation = findAlignedAnnotation(otherCorpus, corpus.getCurrentAnnotation());
            if (otherAnnotation == null) return false;
            if (!tmpPBT.equals(otherAnnotation.getTbTree().toPropbank())
                    || !tmpRS.equals(otherAnnotation.getRoleset())
                    || !tmpT.equals(otherAnnotation.getTbTree().toTextTreeCompact()))
                return false;
        }

		return true;
	}

	@SuppressWarnings("static-access")
	private void menuTbJump() {
		String str = new JOptionPane().showInputDialog(this, "Jump to", "0");
        int maxIndex = corpus.getAnnotation(corpus.getSize() - 1).getIndexInContext();
        if (StringManager.isInteger(str)) {
            int index = Integer.parseInt(str);

            if (0 <= index && index <= maxIndex) {
                if (goToAnnotation(index, -1)) return;
                JOptionPane.showMessageDialog(null,
                        "Utterance " + index + " does not contain any SRL annotations",
                        "Error",
                        JOptionPane.ERROR_MESSAGE);
                return;
            }
        }
        JOptionPane.showMessageDialog(null,
                "Enter a number between 0 and " + maxIndex,
                "Error",
                JOptionPane.ERROR_MESSAGE);
    }

    public boolean goToAnnotation(int index, int predIndex) {
        for (int i = 0; i < corpus.getSize(); i++) {
            if (index == corpus.getAnnotation(i).getIndexInContext()) {
                if (predIndex > 0 && predIndex == corpus.getAnnotation(i).getPredicateIndex()) {
                    comboJump.setSelectedIndex(i);
                    return true;
                }
                else if (predIndex < 0) {
                    comboJump.setSelectedIndex(i);
                    return true;
                }
            }
        }
        return false;
    }

    // ------------------ Menu-Argument Action ------------------

	private boolean menuArgumentArg(ActionEvent e) {
		for (int i=0; i<mbar.argArgs.length; i++) {
            if (e.getSource() == mbar.argArgs[i]) {
                argPanel.updateArg(e.getActionCommand());
				return true;
			}
		}

		return false;
	}

	private boolean menuArgumentFunc(ActionEvent e) {
		for (int i=0; i<mbar.argFunc.length; i++) {
            if (e.getSource() == mbar.argFunc[i]) {
                argPanel.setFunction(e.getActionCommand());
				return true;
			}
		}

		return false;
	}

    private void menuHelpShortcuts() {
        String msg = "Keyboard Shortcuts:\n\n";
        msg += "#### Corpus File Actions\n";
        msg += "Open new file               cmd/ctrl + o \n";
        msg += "Save current file            cmd/ctrl + s \n";
        msg += "\n";
        msg += "#### Navigation\n";
        msg += "Next annotation             .\n";
        msg += "Previous annotation       ,\n";
        msg += "Jump to annotation        cmd/ctrl + j \n";
        msg += "Show incremental IDs     cmd/ctrl + i \n";
        msg += "\n";
        msg += "#### Frameset information\n";
        msg += "Next roleset                  ]\n";
        msg += "Previous roleset            [\n";
        msg += "See examples                cmd/ctrl + e \n";
        msg += "\n";
        msg += "#### Annotation/Editing\n";
        msg += "Edit tree                        cmd/ctrl + t \n";
        msg += "Add predicate               = \n";
        msg += "Add Arg[0-5]                0-5	\n";
        msg += "Add ArgP-Gov               x \n";
        msg += "Add ArgP-Obj                y \n";
        msg += "Approve annotation      cmd/ctrl+shift + a \n";

        JOptionPane.showMessageDialog(this, msg, "Help", JOptionPane.INFORMATION_MESSAGE);
    }

	@SuppressWarnings("static-access")
	private void menuHelpAbout() {
		String msg = str_frameTitle + "\n\n";
		msg += "Jinho D. Choi\n";
		msg += "University of Colorado\n";
		msg += "http://code.google.com/p/propbank/\n\n";
		msg += "and\n\n";
		msg += "Christos Christodoulopoulos\n";
		msg += "University of Illinois\n";
		msg += "https://gitlab-beta.engr.illinois.edu/babysrl-group/jubilee\n\n";

		new JOptionPane().showMessageDialog(this, msg, "About", JOptionPane.INFORMATION_MESSAGE);
	}

    private String getListStr(ArrayList<String[]> items) {
        String listStr = "";
        for (String[] strings : items) {
			listStr += strings[0] + "\t" + strings[1] + " ";
			if (strings.length > 2) {
				for (int i = 2; i < strings.length; i++) listStr += strings[i] + " ";
			}
			listStr = listStr.trim() + "\n";
        }
        return listStr.trim();
    }

    String getTerminalsList() {
        return getListStr(terminalsList);
    }

    String getNonTerminalsList() {
        return getListStr(nonTerminalsList);
    }
}
