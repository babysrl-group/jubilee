/**
* Copyright (c) 2007-2009, Regents of the University of Colorado
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the University of Colorado at Boulder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
package jubilee.toolkit;

import jubilee.awt.JDCTextAreaFrame;
import jubilee.datastructure.JBCorpus;
import jubilee.propbank.PBFrameset;
import jubilee.propbank.PBFramesetReader;
import jubilee.propbank.PBPredicate;
import jubilee.propbank.PBRoleset;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.StringTokenizer;
import java.util.Vector;

@SuppressWarnings("serial")
public class JBFramesetPanel extends JPanel implements ActionListener, ItemListener {
	PBFrameset frameset;
	JComboBox<String> cb_roleset;
	JTextField tf_predicate;
	JTextArea ta_role;
	JButton bt_example, btOpenInBrowser;
	PBFramesetReader fr_reader;
	JBCorpus corpus = null;
	JDCTextAreaFrame fr_example = null;
	JDCTextAreaFrame fr_rolesetComment = null;
	
	private Vector<Integer> vec_pNum, vec_cNum;

	public JBFramesetPanel() {
		setLayout(new BorderLayout());
		addTopPanel();
		addTextField();
		fr_reader = new PBFramesetReader();
		vec_pNum = new Vector<Integer>();
		vec_cNum = new Vector<Integer>();
	}
	
	public void setProperties(String framesetPath)
	{
		fr_reader.setProperties(framesetPath);
	}
	
	// ------------------------- initialize -------------------------
	
	private void addTopPanel() {
		JPanel top = new JPanel();
		top.setLayout(new GridLayout(0,2));
		
		tf_predicate = new JTextField("Predicate");
		tf_predicate.setEditable(false);
		top.add(tf_predicate);
		
		String str[] = {"Roleset"};
		cb_roleset = new JComboBox<String>(str);
		cb_roleset.addItemListener(this);
		top.add(cb_roleset);
		
		bt_example = new JButton("Example");
		bt_example.addActionListener(this);
		bt_example.setEnabled(false);

        btOpenInBrowser = new JButton("View in Browser");
        btOpenInBrowser.addActionListener(this);
        btOpenInBrowser.setEnabled(false);

        JPanel main = new JPanel();
		main.setLayout(new BorderLayout());
		main.add(top, BorderLayout.CENTER);
        JPanel buttons = new JPanel(new BorderLayout());
        buttons.add(bt_example, BorderLayout.NORTH);
        buttons.add(btOpenInBrowser, BorderLayout.SOUTH);
		main.add(buttons, BorderLayout.EAST);
		add(main, BorderLayout.NORTH);
	}
	
	private void addTextField() {
		ta_role = new JTextArea();
		ta_role.setEditable(false);
		JScrollPane sp_role = new JScrollPane(ta_role);
		sp_role.setBorder(new TitledBorder("Roleset Information"));
		sp_role.setHorizontalScrollBarPolicy(ScrollPaneConstants.HORIZONTAL_SCROLLBAR_ALWAYS);
		add(sp_role, BorderLayout.CENTER);
	}
	
	// ---------------------- utilties ----------------------
	
	// called from PBToolKit
	public void setCorpus(JBCorpus corpus) {
		this.corpus = corpus;
	}
	
	public String getRoleset() {
		return (String)cb_roleset.getSelectedItem();
	}
	
	public void prevRoleset() {
		int index = cb_roleset.getSelectedIndex() - 1;
		
		if (index >= 0)
		cb_roleset.setSelectedIndex(index);
	}
	
	public void nextRoleset() {
		int index = cb_roleset.getSelectedIndex() + 1;
		
		if (index != 0 && index < cb_roleset.getItemCount())
			cb_roleset.setSelectedIndex(index);
	}
	
	// ---------------------- Events ---------------------- 
	
	public void itemStateChanged(ItemEvent e) {
		if (cb_roleset.getSelectedIndex() < 0)	return;
        // We have frameset information for cb_roleset items [1, N-4] (since we have four "null" elements)
		if (cb_roleset.getSelectedIndex() != 0 && cb_roleset.getSelectedIndex() < cb_roleset.getItemCount() - 4) {
            PBPredicate predicate = frameset.getPredicate(getPredicateNum());
			PBRoleset roleset = predicate.getRoleset(getRolesetNum());
			
			tf_predicate.setText(predicate.getLemma());
			ta_role.setText(roleset.toString());
			bt_example.setEnabled(true);
		}
		else {
            tf_predicate.setText("");
			ta_role.setText("");
			bt_example.setEnabled(false);
		}

        if (vec_cNum.size() == 0 || frameset.getPredicate(vec_pNum.get(0)).isPrep())
            btOpenInBrowser.setEnabled(false);
        else
            btOpenInBrowser.setEnabled(Desktop.isDesktopSupported());
		
		corpus.getCurrentAnnotation().setRoleset((String) cb_roleset.getSelectedItem());
	}
	
	public void actionPerformed(ActionEvent e) {
        if (e.getSource() == bt_example)
		    showExample();
        else if (e.getSource() == btOpenInBrowser)
            openInBrowser();
	}

	public void showExample() {
		PBPredicate predicate = frameset.getPredicate(getPredicateNum());
		PBRoleset roleset = predicate.getRoleset(getRolesetNum());
		
		if (fr_example != null)
			fr_example.dispose();
		fr_example = new JDCTextAreaFrame("Examples for "+roleset.getId(), roleset.getExamples());
	}

    private void openInBrowser() {
        PBPredicate predicate = frameset.getPredicate(vec_pNum.get(0));
        try {
            String url = "http://verbs.colorado.edu/propbank/framesets-english/" + predicate.getLemma() + "-v.html";
            Desktop.getDesktop().browse(new URI(url));
        } catch (IOException e) {
            e.printStackTrace();
        } catch (URISyntaxException e) {
            e.printStackTrace();
        }
    }

	public void viewRolesetComment() {
		PBPredicate predicate = frameset.getPredicate(getPredicateNum());
		PBRoleset roleset = predicate.getRoleset(getRolesetNum());
		
		fr_rolesetComment = new JDCTextAreaFrame("Comments for "+roleset.getId(), roleset.getComment());
	}

	public void updateFrameset(String frame, String roleset) {
		StringTokenizer tok = new StringTokenizer(roleset, ".");
		String lemma = tok.nextToken();
		
		// remove previous settings
		cb_roleset.removeAllItems();
		vec_pNum.removeAllElements();
		vec_cNum.removeAllElements();
		
		int k = 0;
		cb_roleset.insertItemAt(lemma+".XX", k++);
		if ((frameset = fr_reader.getFrameset(frame)) != null) {
            for (int i=0; i<frameset.getSize(); i++) {
                PBPredicate predicate = frameset.getPredicate(i);
				String[] rolesets = predicate.getRolesetID();
				for (int j=0; j<rolesets.length; j++) {
                    cb_roleset.insertItemAt(rolesets[j], k++);
					vec_pNum.add(i);	vec_cNum.add(j);
				}
			}
            cb_roleset.insertItemAt(lemma+".YY", k++);
            cb_roleset.insertItemAt(lemma+".ER", k++);
            cb_roleset.insertItemAt(lemma+".LV", k++);
            cb_roleset.insertItemAt(lemma+".IE", k);
			
			int index = getItemIndex(roleset);
			if (index != -1)	cb_roleset.setSelectedIndex(index);
			else				cb_roleset.setSelectedIndex(0);
		}
		else {
            cb_roleset.setSelectedIndex(0);
			tf_predicate.setText("No frameset-file");
		}
	}
	
	private int getItemIndex(String lemma) {
		for (int i=0; i<cb_roleset.getItemCount(); i++)
			if (lemma.equalsIgnoreCase(cb_roleset.getItemAt(i)))	return i;
		
		return -1;
	}
	
	private int getPredicateNum() {
		return vec_pNum.get(cb_roleset.getSelectedIndex()-1);
	}
	
	private int getRolesetNum()
	{
		return vec_cNum.get(cb_roleset.getSelectedIndex()-1);
	}
}
