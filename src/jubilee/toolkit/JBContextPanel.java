package jubilee.toolkit;

import jubilee.datastructure.JBCorpus;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

class JBContextPanel extends JScrollPane {
    private JList<String> contextList = new JList<>();
    private int maxContext = 0;

    JBContextPanel(final JBToolkit parent) {
        MouseAdapter doubleClickListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    String indexStr = contextList.getSelectedValue().split(" ")[0];
                    if (indexStr.equals("##")) return;
                    int index = Integer.parseInt(indexStr);
                    parent.goToAnnotation(index, -1);

                } else super.mouseClicked(e);
            }
        };
        contextList.addMouseListener(doubleClickListener);
        contextList.setVisibleRowCount(13);
        viewport.setView(contextList);
    }

    void buildContextList(JBCorpus corpus) {
        List<String> contexts = corpus.getContexts();
        String[] listData = new String[contexts.size()];
        for (int contextInd = 0; contextInd < contexts.size(); contextInd++) {
            String context = contexts.get(contextInd);
            if (corpus.getAnnotationsFromContext(contextInd) == null)
                listData[contextInd] = "## " + context + "\n";
            else listData[contextInd] = context;
        }
        contextList.setListData(listData);
        maxContext = contexts.size();
    }

    public void setPosition(int index) {
        contextList.setSelectedIndex(index);
        contextList.ensureIndexIsVisible(index + Math.min(6, maxContext - index - 1));
        contextList.ensureIndexIsVisible(index - Math.min(6, index));
    }
}
