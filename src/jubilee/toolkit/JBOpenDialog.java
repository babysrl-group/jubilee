/**
* Copyright (c) 2007, Regents of the University of Colorado
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the University of Colorado at Boulder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
package jubilee.toolkit;

import jubilee.util.DataManager;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.util.*;
import java.util.List;

/**
 * @author Jinho D. Choi
 * <b>Last update:</b> 5/6/2010
 */
@SuppressWarnings("serial")
public class JBOpenDialog extends JDialog implements ActionListener, ItemListener, ListSelectionListener
{
	private JBToolkit              jbtk;
	private JComboBox<String> cb_projects;
	private JList<String> ls_newTask = null;
	private JList<String> ls_myTask  = null;
	private DefaultListModel<String> lm_newTask;
	private DefaultListModel<String> lm_myTask;
	private JButton                bt_cancel;
	private JButton                bt_enter;
	private HashMap<String,String> m_dataset;
	private int                    i_maxAnn;
	
	/**
	 * Initializes the open-dialog.
	 * @param jbtk Jubilee's main window (JBToolKit).
	 * @param isCancel if (isCancel == true/false) enable/disable the cancel button.
	 */
	public JBOpenDialog(JBToolkit jbtk, boolean isCancel, int maxAnn) {
		super(jbtk, "Open a task", true);
		this.jbtk = jbtk;
		i_maxAnn  = maxAnn;
		
		Container cp = getContentPane();
		initComponents(cp, isCancel);
		initBounds(cp);

        if (isCancel)
		    setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        else {
            setDefaultCloseOperation(JDialog.DO_NOTHING_ON_CLOSE);
            addWindowListener(new WindowAdapter() {
                @Override
                public void windowClosing(WindowEvent e) {
                    System.exit(0);
                }
            });
        }
        setTitle(jbtk.getUserID().toUpperCase() + ": " + getTitle());

		setBounds(10, 10, 350, 600);
		setVisible(true);
	}
	
	// --------------------------- init*() --------------------------- 
	
	private void initComponents(Container cp, boolean isCancel) {
		// combobox: dataset
		cb_projects = new JComboBox<String>(DataManager.getProjects());
		cb_projects.setBorder(new TitledBorder("Choose a project"));
		cb_projects.addItemListener(this);
		
		ArrayList<String[]> list = DataManager.getContents(cb_projects.getItemAt(jbtk.i_currSetting) + DataManager.PATH_FILE_EXT);
		m_dataset = getMap(list);
		
		// lists new and my tasks
		lm_newTask = new DefaultListModel<String>();
		ls_newTask = new JList<String>(lm_newTask);
		ls_newTask.setBorder(new TitledBorder("New Tasks"));
		
		lm_myTask = new DefaultListModel<String>();
		ls_myTask = new JList<String>(lm_myTask);
		ls_myTask.setBorder(new TitledBorder("My Tasks"));

        addListListeners();

		initJList();
		
		// button: cancel, enter
		bt_cancel = getJButton(cp, "Cancel");
		bt_cancel.setEnabled(isCancel);
		bt_enter = getJButton(cp, "Enter");
		
		cb_projects.setSelectedIndex(jbtk.i_currSetting);
	}
	
	private HashMap<String, String> getMap(ArrayList<String[]> list) {
		HashMap<String, String> map = new HashMap<String, String>();
		for (String[] arr : list)	map.put(arr[0], arr[1]);
		
		return map;
	}

    private void addListListeners() {
        MouseAdapter doubleClickListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    actionBtEnter();
                } else super.mouseClicked(e);
            }
        };
        ls_newTask.addListSelectionListener(this);
        ls_newTask.addMouseListener(doubleClickListener);
        ls_myTask.addListSelectionListener(this);
        ls_myTask.addMouseListener(doubleClickListener);
    }
	
	private JButton getJButton(Container cp, String title) {
		JButton bt = new JButton(title);
		
		bt.addActionListener(this);
		cp.add(bt);
		return bt;
	}
	
	private void initJList() {
		// get file lists
		File taskDir = new File(m_dataset.get(DataManager.TASK));
		File annDir  = new File(m_dataset.get(DataManager.ANNOTATION));
		String[] tasklist = taskDir.list();	Arrays.sort(tasklist);
		
		List     <String> tmp1 = Arrays.asList(annDir.list());
		ArrayList<String> tmp2 = new ArrayList<String>();
		for (String str : tmp1)
			if (jbtk.isGold() || !str.endsWith(".gold"))
                tmp2.add(str);
		
		String[] annlist = new String[tmp2.size()];
		tmp2.toArray(annlist);	Arrays.sort(annlist);
		
		// remove previous lists
		lm_newTask.removeAllElements();
		lm_myTask .removeAllElements();
		
		// add new task lists
        for (String aTasklist : tasklist) {
            String suffix = "";
            if (jbtk.isGold()) {
                // Add the number of existing annotations for each task
                int annotations = countAnnotations(aTasklist, tmp2);
                if (annotations > 0) suffix = " (" + annotations + ")";
            }
            if (aTasklist.substring(aTasklist.lastIndexOf(".") + 1).equalsIgnoreCase("task"))
                lm_newTask.addElement(aTasklist + suffix);
        }
		
		Vector<String> vec = new Vector<String>();
		// add ann file list and remove the corresponding task list
        for (String anAnnlist : annlist) {
            int index = anAnnlist.lastIndexOf(".");
            String task = anAnnlist.substring(0, index);
            String id = anAnnlist.substring(index + 1);
            vec.add(task);

            if (id.equalsIgnoreCase(jbtk.getUserID())) {
                if (jbtk.isGold()) {
                    // Add the number of existing annotations for each task
                    int annotations = countAnnotations(task, tmp2);
                    if (annotations > 0) task += " (" + annotations + ")";
                }
                lm_newTask.removeElement(task);
                lm_myTask.addElement(anAnnlist);
            } else if (!jbtk.isGold()) {
                int count = 1, j = vec.indexOf(task, 0);
                while ((j = vec.indexOf(task, ++j)) != -1) count++;

                if (count >= i_maxAnn) lm_newTask.removeElement(task);
            }
        }
	}

    private int countAnnotations(String task, List<String> annlist) {
        int count = 0;
        for (String anAnnlist : annlist) {
            if (anAnnlist.substring(0, anAnnlist.lastIndexOf(".")).equals(task))
                count++;
        }
        return count;
    }
	
	private void initBounds(Container cp) {
		cp.setLayout(new BorderLayout());
		
		cp.add(cb_projects, BorderLayout.NORTH);
		
		JPanel pnC = new JPanel();
		pnC.setLayout(new GridLayout(0,2));
		pnC.add(new JScrollPane(ls_newTask));
		pnC.add(new JScrollPane(ls_myTask));
		cp.add(pnC, BorderLayout.CENTER);
		
		JPanel pnS = new JPanel();
		pnS.setLayout(new GridLayout(0,2));
		pnS.add(bt_cancel);
		pnS.add(bt_enter);
		cp.add(pnS, BorderLayout.SOUTH);
	}
	
	// --------------------------- Events ---------------------------
	
	/* (non-Javadoc)
	 * @see javax.swing.event.ListSelectionListener#valueChanged(javax.swing.event.ListSelectionEvent)
	 */
	public void valueChanged(ListSelectionEvent e) {
		if (e.getSource() == ls_newTask)
			ls_myTask.clearSelection();
		else if (e.getSource() == ls_myTask)
			ls_newTask.clearSelection();
	}

	/* (non-Javadoc)
	 * @see java.awt.event.ItemListener#itemStateChanged(java.awt.event.ItemEvent)
	 */
	public void itemStateChanged(ItemEvent e) {
		if (e.getSource() == cb_projects) {
            ArrayList<String[]> list = DataManager.getContents(cb_projects.getSelectedItem() + DataManager.PATH_FILE_EXT);
			m_dataset = getMap(list);
			initJList();
		}
	}
	
	/* (non-Javadoc)
	 * @see java.awt.event.ActionListener#actionPerformed(java.awt.event.ActionEvent)
	 */
	public void actionPerformed(ActionEvent e) {
		if      (e.getSource() == bt_cancel)	dispose();
		else if (e.getSource() == bt_enter)		actionBtEnter();
	}

	private void actionBtEnter() {
		if (ls_newTask.isSelectionEmpty() && ls_myTask.isSelectionEmpty())	return;
        jbtk.i_currSetting = cb_projects.getSelectedIndex();
        StringTokenizer tok = new StringTokenizer((String)cb_projects.getSelectedItem(), ".");
        String corpusStr = tok.nextToken();
		jbtk.initProperties(corpusStr, m_dataset);
        jbtk.readReferenceMaterial();

        try {
            if (!ls_newTask.isSelectionEmpty()) {
                String value = ls_newTask.getSelectedValue();
                // Remove the (#) if any
                value = value.replaceAll(" \\([0-9]\\)", "");
                String[] tmp = getFileList(value, true);
                if (tmp != null) {
                    setVisible(false);
                    jbtk.openFile(tmp, true);
                }
                else
                    return;
            } else {
                String[] tmp = getFileList(ls_myTask.getSelectedValue(), false);
                setVisible(false);
                jbtk.openFile(tmp, false);
            }
        }
        catch (Exception e) {
            JOptionPane.showMessageDialog(null, "There was an error: \n" + e.getMessage(), "Error reading the corpus",
                    JOptionPane.ERROR_MESSAGE);
            return;
        }
		dispose();
	}
	
	@SuppressWarnings("static-access")
	private String[] getFileList(String fstFile, boolean isNew) {
		Vector<String> vec = new Vector<String>();
		String task = (isNew) ? fstFile : fstFile.substring(0, fstFile.lastIndexOf("."));

		if (jbtk.isGold()) {
            File annDir = new File(m_dataset.get(DataManager.ANNOTATION));
			String[] annlist = annDir.list();	Arrays.sort(annlist);

            for (String anAnnlist : annlist) {
                String myTask = anAnnlist.substring(0, anAnnlist.lastIndexOf("."));
                if (myTask.equals(task) && !anAnnlist.equals(fstFile))
                    vec.add(anAnnlist);
            }
			
			if (isNew && vec.size() == 0) {
                new JOptionPane().showMessageDialog(this, "No annotations for the task");
				return null;
			}
		}
		
		String[] tmp = new String[1+vec.size()];
		tmp[0] = task;
		for (int i=0; i<vec.size(); i++)	tmp[i+1] = vec.get(i);
		
		return tmp;
	}
}
