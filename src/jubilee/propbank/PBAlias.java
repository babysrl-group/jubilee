package jubilee.propbank;

/**
 * PBAlias contains 'framenet', 'verbnet', 'pos' and the 'alias' itself
 */
public class PBAlias {
    //TODO How should 'framenet' and 'verbnet' attributes be represented?
    private String framenet, verbnet, pos, alias;

    public PBAlias(String framenet, String verbnet, String pos, String alias) {
        this.framenet = framenet;
        this.verbnet = verbnet;
        this.pos = pos;
        this.alias = alias;
    }

    public String toString() {
        return pos + "-alias: " + alias;
    }
}
