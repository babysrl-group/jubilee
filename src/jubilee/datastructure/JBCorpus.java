package jubilee.datastructure;

import jubilee.toolkit.JBToolkit;
import jubilee.util.DataManager;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.swing.*;
import java.io.*;
import java.util.*;

/**
 * The Jubilee corpus for each annotation task. It consists of a list of {@link JBDataStructure}s
 * and various pointers to the one being currently edited.
 */
public class JBCorpus {

    private List<JBDataStructure> annotations;
    private List<String> contexts;
    private Map<Integer, List<JBDataStructure>> contextToAnnotation;
    private Map<String, String> bookmarks;

    private int lastEditIndex;

    private JBDataStructure currentAnnotationOriginal;
    private String corpusFile;
    private String bookmarksFileExt = "-bmarks.txt";

    private int currentAnnotationIndex;

    private String annotator;

    private boolean annotationFinished;

    public JBCorpus(String file, String annotator) throws Exception {
        this.annotator = annotator;
        annotations = new ArrayList<>();
        contexts = new ArrayList<>();
        contextToAnnotation = new HashMap<>();
        bookmarks = new HashMap<>();
        corpusFile = file;
        currentAnnotationIndex = 0;
        currentAnnotationOriginal = new JBDataStructure();
        readCorpus();
    }

    /**
     * A constructor used in "gold" mode. It uses a set of corpora to initialise the gold corpus.
     * This is needed in case of missing/added labels in any of the corpora.
     */
    public JBCorpus(JBCorpus[] corpora) {
        annotator = DataManager.GOLD_ID;
        // Make sure that the contexts match across corpora
        int contextLength = corpora[0].contexts.size();
        for (JBCorpus corpus : corpora) {
            if (corpus.contexts.size() != contextLength) {
                System.err.println("Contexts don't match across corpora");
                System.exit(-1);
            }
        }
        // Now just take the contexts from the first corpus
        contexts = corpora[0].contexts;
        currentAnnotationIndex = 0;
        currentAnnotationOriginal = new JBDataStructure();
        bookmarks = new HashMap<>();

        // Aggregate the annotations across corpora
        contextToAnnotation = new HashMap<>();
        annotations = new ArrayList<>();
        for (int i = 0; i < contexts.size(); i++) {
            for (JBCorpus corpus : corpora) {
                if (!corpus.contextToAnnotation.containsKey(i)) continue;
                List<JBDataStructure> tempOtherList = corpus.contextToAnnotation.get(i);
                List<JBDataStructure> tempList;
                if (!contextToAnnotation.containsKey(i))
                    tempList = new ArrayList<>();
                else tempList = contextToAnnotation.get(i);
                for (JBDataStructure otherAnn : tempOtherList) {
                    if (!existsInList(tempList, otherAnn)) {
                        tempList.add(otherAnn);
                        annotations.add(otherAnn);
                    }
                }
                contextToAnnotation.put(i, tempList);
            }
        }
    }

    private boolean existsInList(List<JBDataStructure> annotationList, JBDataStructure otherAnnotation) {
        for (JBDataStructure ann : annotationList) {
            if (!ann.getType().equals(otherAnnotation.getType())) continue;
            if (ann.getPredicateIndex() == otherAnnotation.getPredicateIndex())
                return true;
            String annTree = ann.getTbTree().toTextTreeCompact();
            String otherAnnTree = otherAnnotation.getTbTree().toTextTreeCompact();
            // Special case: check for presence of traces
            if (annTree.contains("*") || otherAnnTree.contains("*")) {
                int countAnn = annTree.length() - annTree.replace("*", "").length();
                int countOtherAnn = otherAnnTree.length() - otherAnnTree.replace("*", "").length();
                // TODO: 2/4/16 This is potentially dangerous but should do for now
                if (Math.abs(ann.getPredicateIndex() - otherAnnotation.getPredicateIndex()) == countAnn+countOtherAnn)
                    return true;
            }
        }
        return false;
    }

    private void readCorpus() throws Exception {
        BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(corpusFile)));
        String fileString = "";
        String line;
        while ((line = in.readLine()) != null) {
            fileString += line;
        }

        JSONObject corpus = new JSONObject(fileString);
        lastEditIndex = corpus.getInt("lastEditIndex");
        JSONArray annotationsArray = corpus.getJSONArray("annotations");
        for (int i = 0; i < annotationsArray.length(); i++) {
            JSONObject annotation = annotationsArray.getJSONObject(i);
            String treeString = annotation.getString("tree");
            String pbInstanceString = annotation.getString("pbInstance");
            int position = annotation.getInt("indexInContext");
            JBDataStructure structure = new JBDataStructure(treeString, pbInstanceString, position);
            annotations.add(structure);
            List<JBDataStructure> annotationList;
            if (contextToAnnotation.containsKey(position))
                annotationList = contextToAnnotation.get(position);
            else annotationList = new ArrayList<>();
            annotationList.add(structure);
            contextToAnnotation.put(position, annotationList);
        }

        JSONArray contextsArray = corpus.getJSONArray("contexts");
        for (int i = 0; i < contextsArray.length(); i++) {
            String context = contextsArray.getString(i);
            contexts.add(context);
        }


        String bookmarksFile = corpusFile + bookmarksFileExt;
        if (new File(bookmarksFile).exists()) {
            in = new BufferedReader(new InputStreamReader(new FileInputStream(bookmarksFile)));
            while ((line = in.readLine()) != null) {
                String[] parts = line.split("#");
                bookmarks.put(parts[0], parts[1]);
            }
        }
    }

    public void saveCorpus(String file) throws JSONException, IOException {
        JSONObject corpus = new JSONObject();
        corpus.put("lastEditIndex", lastEditIndex);
        JSONArray annotationsArray = new JSONArray();
        for (JBDataStructure structure : annotations) {
            JSONObject annotation = new JSONObject();
            annotation.put("tree", structure.getTbTree().toTextTreeCompact());
            annotation.put("pbInstance", structure.toProbankString());
            annotation.put("indexInContext", structure.getIndexInContext());
            annotationsArray.put(annotation);
        }
        corpus.put("annotations", annotationsArray);
        JSONArray contextsArray = new JSONArray();
        for (String context : contexts) {
            contextsArray.put(context);
        }
        corpus.put("contexts", contextsArray);

        FileWriter out = new FileWriter(file);
        out.write(corpus.toString(2));
        out.close();

        out = new FileWriter(file + bookmarksFileExt);
        for (String bookmark : bookmarks.keySet()) {
            out.write(bookmark + "#" + bookmarks.get(bookmark) + System.lineSeparator());
        }
        out.close();
    }

    private void setLastEditIndex(int index) {
        lastEditIndex = index;
    }

    public void setCurrentAnnotationIndex(int annotationIndex, boolean approved) {
        // Before moving on, confirm that we want to keep the changes to the current annotation
        // However, even if the annotation hasn't changed but the user chose to approve it, we should mark it as done
        if (getCurrentAnnotation() != null)
            if (getCurrentAnnotation().isChanged() || approved)
                confirmChanges(approved);
        this.currentAnnotationIndex = annotationIndex;
    }

    private void confirmChanges(boolean approved) {
        boolean save;
        if (approved) save = true;
        else {
            int response = JOptionPane.showConfirmDialog(null, "Do you want to keep the changes to the annotation?",
                    "Confirm", JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);
            save = JBToolkit.isYes(response);
        }
        if (!save) {
            try {
                revertChanges();
            }
            catch (Exception e) {
                JOptionPane.showMessageDialog(null, "There was an error: \n" + e.getMessage(), "Error",
                        JOptionPane.ERROR_MESSAGE);
            }
        }
        else {
            getCurrentAnnotation().setAnnotator(annotator);
            getCurrentAnnotation().hasChanged(false);
            setLastEditIndex(getCurrentAnnotationIndex());
        }
    }

    public void copyCurrent(JBDataStructure otherAnnotation) {
        JBDataStructure newAnnotation = new JBDataStructure();
        contextToAnnotation.get(otherAnnotation.getIndexInContext()).remove(getCurrentAnnotation());
        newAnnotation.copyFrom(otherAnnotation, false);
        annotations.set(currentAnnotationIndex, newAnnotation);
        contextToAnnotation.get(newAnnotation.getIndexInContext()).add(newAnnotation);
    }

    private void revertChanges() throws Exception {
        // Make sure that there is something to revert back to
        if (currentAnnotationOriginal == null) throw new Exception("Original annotation is missing");
        annotations.set(currentAnnotationIndex, currentAnnotationOriginal);
    }

    public void removeCurrentAnnotation() {
        JBDataStructure currentAnnotation = getCurrentAnnotation();
        int indexInContext = currentAnnotation.getIndexInContext();
        List<JBDataStructure> annotationsForContext = contextToAnnotation.get(indexInContext);
        if (annotationsForContext.size() == 1) contextToAnnotation.remove(indexInContext);
        else {
            annotationsForContext.remove(currentAnnotation);
            contextToAnnotation.put(indexInContext, annotationsForContext);
        }
        annotations.remove(currentAnnotationIndex);
    }

    public void addAnnotation(String annotator, String lemma) throws Exception {
        JBDataStructure current = getCurrentAnnotation();
        // Start from copying the current annotation
        String framesetLemma = lemma;
        if (lemma.contains("-p"))
            framesetLemma = lemma.split("-")[0];
        String pbInstanceString = annotator + " " + lemma + " " + framesetLemma + ".XX#";
        JBDataStructure newAnnotation = new JBDataStructure(current.getTbTree().toTextTree(), pbInstanceString,
                current.getIndexInContext());
        annotations.add(currentAnnotationIndex, newAnnotation);
        contextToAnnotation.get(current.getIndexInContext()).add(newAnnotation);
    }

    public void backup(JBDataStructure currentAnnotation) {
        // If this is the first type we visit this annotation, make a copy
        if (currentAnnotationOriginal.getIndexInContext() != currentAnnotation.getIndexInContext()) {
            currentAnnotationOriginal = new JBDataStructure();
            currentAnnotationOriginal.copyFrom(currentAnnotation, true);
        }
    }

    // --------------------- Getters --------------------- //

    public JBDataStructure getAnnotation(int index) {
        return annotations.get(index);
    }

    public int getSize() {
        return annotations.size();
    }

    public int getLastEditIndex() {
        return lastEditIndex;
    }

    public int getCurrentAnnotationIndex() {
        return currentAnnotationIndex;
    }

    public String getContext(int index) {
        return contexts.get(index);
    }

    public JBDataStructure getCurrentAnnotation() {
        // In case we just deleted the last annotation
        if (currentAnnotationIndex >= annotations.size()) return null;
        return getAnnotation(currentAnnotationIndex);
    }
    public List<String> getContexts() {
        return contexts;
    }

    public List<JBDataStructure> getAnnotationsFromContext(int contextInd) {
        return contextToAnnotation.get(contextInd);
    }

    public int getAnnotationPosition(JBDataStructure annotation) {
        List<JBDataStructure> otherAnnotations = getAnnotationsFromContext(annotation.getIndexInContext());
        int position = 0;
        for (JBDataStructure other : otherAnnotations) {
            position++;
            if (other.getPredicateIndex() == annotation.getPredicateIndex() &&
                    other.getRoleset().equals(annotation.getRoleset())) break;
        }
        return position;
    }

    public boolean isAnnotationFinished() {
        return annotationFinished;
    }

    public void setAnnotationFinished(boolean annotationFinished) {
        this.annotationFinished = annotationFinished;
    }

    /**
     * Adds a bookmark to the current annotation
     */
    public void addBookmark(String message) {
        bookmarks.put(generateBookmarkKey(), message);
    }

    /**
     * Removes an existing bookmark from the current annotation
     */
    public void removeBookmark() {
        String key = generateBookmarkKey();
        if (bookmarks.containsKey(key))
            bookmarks.remove(key);
    }

    public boolean isBookmarked() {
        String key = generateBookmarkKey();
        return  (bookmarks.containsKey(key));
    }

    private String generateBookmarkKey() {
        JBDataStructure annotation = getCurrentAnnotation();
        return annotation.getIndexInContext() + ":" + annotation.getPredicateIndex();
    }

    public Map<String, String> getBookmarks() {
        return bookmarks;
    }
}
