package jubilee.datastructure;

import jubilee.toolkit.JBToolkit;
import jubilee.treebank.TBNode;
import jubilee.treebank.TBTree;
import jubilee.util.DataManager;
import jubilee.util.FileTokenizer;

import javax.swing.*;
import java.util.StringTokenizer;

/**
 * The Jubilee data structure for each utterance in the {@link JBCorpus}.
 * It contains the syntactic tree, the propbank annotation and a pointer to the context.
 */
public class JBDataStructure {
    /** Left Round Bracket "(" */
    static final public String LRB = "(";
    /** Right Round Bracket ")" */
    static final public String RRB = ")";
    static final public String HEAD = "TOP";
    /** argument-joiner used in annotation files */
    public static final String ARG_JOINER = "-";
    /** annotation functions */
    public static final String ANT_FUNC = "*,;&";
    /** argument tag for relation */
    public static String REL = "rel";

    /** The indexInContext of the annotation in the context */
    private int indexInContext;

    /** The start token span of the predicate */
    private int predicateIndex;

    /** Indicates that some part of the structure has changed (to prompt for a save dialogue) */
    private boolean changed;

    private String annotator;
    private String type;
    private String roleset;
    private TBTree tbTree;

    /** Dummy constructor used for the backup of annotations (see {@link JBCorpus#backup(JBDataStructure)}) */
    public JBDataStructure() {
        this.indexInContext = -1;
        this.changed = false;
        this.annotator = "";
    }

    public JBDataStructure(String treeString, String pbInstanceString, int indexInContext) throws Exception {
        this.indexInContext = indexInContext;
        tbTree = readTBTree(treeString);
        readPBInstance(pbInstanceString);
        this.changed = false;
    }

    public TBTree readTBTree(String treeString) throws Exception {
        String delim = LRB + RRB + FileTokenizer.WHITE;
        StringTokenizer tokenizer = new StringTokenizer(treeString, delim, true);
        String str;
        do {
            str = nextToken(tokenizer);
            if (str == null) return null;
        }
        while(!str.equals(LRB));

        int numBracket = 1;
        TBNode head = new TBNode(null, HEAD);
        TBNode curr = head;

        do {
            if ((str = nextToken(tokenizer)) == null)
                throw new Exception("Error reading the tree");

            if (numBracket == 1 && str.equals(HEAD))	continue;

            if (str.equals(LRB)) {
                numBracket++;
                if ((str = nextToken(tokenizer)) == null)
                    throw new Exception("Tag is missing");

                TBNode node = new TBNode(curr, str);
                curr.addChild(node);
                curr = node;
            }
            else if (str.equals(RRB)) {
                numBracket--;
                curr = curr.getParent();
            }
            else
                curr.setWord(str);
        }
        while (numBracket > 0);

        return new TBTree(head);
    }

    // skips all white-spaces and returns the next token.
    private String nextToken(StringTokenizer tok) {
        while (tok.hasMoreTokens()) {
            String str = tok.nextToken();
            if (!FileTokenizer.WHITE.contains(str))
                return str;
        }

        return null;
    }

    private void readPBInstance(String pbInstanceString) {
        String[] split = pbInstanceString.split("#");
        String info = split[0];
        String labels = null;
        if (split.length > 1)
            labels = split[1];

        StringTokenizer tok = new StringTokenizer(info);

        annotator = tok.nextToken();
        type      = tok.nextToken();
        roleset   = tok.nextToken();

        if (labels != null)
            readPropbankLabels(labels, tbTree);
    }

    private void readPropbankLabels(String labels, TBTree tree) {
        StringTokenizer tok = new StringTokenizer(labels);
        while (tok.hasMoreTokens()) {
            StringTokenizer tok_termInfo = new StringTokenizer(tok.nextToken(), ARG_JOINER);
            StringTokenizer tok_description = new StringTokenizer(tok_termInfo.nextToken(), ":" + ANT_FUNC, true);

            String arg = tok_termInfo.nextToken();	// get argument type
            while (tok_termInfo.hasMoreTokens())
                arg += "-" + tok_termInfo.nextToken();

            String symbol = "", loc = "";
            while (tok_description.hasMoreTokens()) {
                int terminalIdx = Integer.parseInt(tok_description.nextToken());
                tok_description.nextToken();	// :
                int height = Integer.parseInt(tok_description.nextToken());

                tree.moveTo(terminalIdx, height);
                loc += symbol + terminalIdx + ":" + height;
                if (arg.equals(REL))
                    predicateIndex = terminalIdx;
                tree.setArg(loc, arg);

                if (tok_description.hasMoreTokens())		// get symbol
                    symbol = tok_description.nextToken();
            }
        }
    }

    public void setTree(String treeString) throws Exception {
        TBTree newTree = readTBTree(treeString);
        // Add the propbank annotation to the new tree
        readPropbankLabels(tbTree.toPropbank(), newTree);
        if (!tbTree.equals(newTree)) {
            tbTree = newTree;
            hasChanged(true);
        }
        else {
            JOptionPane.showMessageDialog(null, "No changes detected", "Information",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void setTree(String treeString, int offset, int newWordIndex) throws Exception {
        TBTree newTree = readTBTree(treeString);
        // Add the propbank annotation to the new tree including the new indices
        String oldPropbankLabels[] = tbTree.toPropbank().split("\\s+");
        String newLabels = "";
        for (String label : oldPropbankLabels) {
            int index = Integer.parseInt(label.split(":")[0]);
            if (index >= newWordIndex)
                index += offset;
            newLabels += index + ":" + label.split(":")[1] + " ";
        }

        readPropbankLabels(newLabels, newTree);
        if (!tbTree.equals(newTree)) {
            tbTree = newTree;
            hasChanged(true);
        }
        else {
            JOptionPane.showMessageDialog(null, "No changes detected", "Information",
                    JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void setRoleset(String roleset) {
        if (!this.roleset.equals(roleset)) {
            this.roleset = roleset;
            // If the role is set to XX it means that we couldn't find the predicate in the frameset,
            // so don't mark it as a change
            if (!roleset.endsWith("XX"))
                hasChanged(true);
        }
    }

    public void setType(String type) {
        if (!this.type.equals(type)){
            this.type = type;
            hasChanged(true);
        }
    }

    public void setAnnotator(String annotator) {
        if (!this.annotator.equals(annotator)) {
            this.annotator = annotator;
            if (!annotator.equals(DataManager.GOLD_ID))
                hasChanged(true);
        }
    }

    public void copyFrom(JBDataStructure other, boolean copyAnnotator) {
        this.indexInContext = other.indexInContext;
        this.predicateIndex = other.predicateIndex;
        this.tbTree = other.tbTree.copy();
        this.type = other.type;
        this.roleset = other.roleset;
        if (copyAnnotator)
            this.annotator = other.annotator;
    }

    public void hasChanged(boolean changed) {
        this.changed = changed;
        if (changed) {
            String title = JBToolkit.getInstance().getTitle();
            if (!title.startsWith(JBToolkit.EDITED))
                JBToolkit.getInstance().setTitle(JBToolkit.EDITED + title);
        }
    }

    public String toProbankString() {
        return annotator + " " + type + " " + roleset + "#" + tbTree.toPropbank();
    }

    // --------------------- Getters --------------------- //

    public TBTree getTbTree() {
        return tbTree;
    }
    public int getIndexInContext() {
        return indexInContext;
    }
    public int getPredicateIndex() {
        return predicateIndex;
    }
    public String getType() {
        return type;
    }
    public String getRoleset() {
        return roleset;
    }
    public String getAnnotator() {
        return annotator;
    }
    public boolean isChanged() {
        return changed;
    }
    public String getPropbankTree() {
        return tbTree.toPropbank();
    }
}
