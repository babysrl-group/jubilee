/**
* Copyright (c) 2007, Regents of the University of Colorado
* All rights reserved.
*
* Redistribution and use in source and binary forms, with or without
* modification, are permitted provided that the following conditions are met:
*
* Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
* Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
* Neither the name of the University of Colorado at Boulder nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
*
* THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
* AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
* IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
* ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
* LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
* CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
* SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
* INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
* CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
* ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
* POSSIBILITY OF SUCH DAMAGE.
*/
package jubilee.util;

import java.io.*;

/**
 * @author Jinho D. Choi
 * <b>Last update:</b> 5/6/2010
 */
public class StringManager
{
	static public String addIndent(String str, int length)
	{
		StringBuilder build = new StringBuilder();

		build.append(str);
		for (int i=str.length(); i<length; i++)	build.append(" ");
		
		return build.toString();
	}

    static public boolean isInteger(String str)
	{
		try
		{
            //noinspection ResultOfMethodCallIgnored
            Integer.parseInt(str);
        }
		catch (NumberFormatException e){return false;}
		
		return true;
	}

	static public String getUTF8(String str)
	{
		String utf = "";
		
		try
		{
			utf = new String(str.getBytes(), "UTF-8");
		}
		catch (UnsupportedEncodingException e) {e.printStackTrace();}
		
		return utf; 
	}

    public static int findMatchingBracket(String doc, int offset) {
        if (doc.length() == 0)
            return -1;
        char c = doc.charAt(offset);
        char cprime; // c` - corresponding character
        boolean direction; // true = back, false = forward

        switch(c) {
            case '(' :
                cprime = ')';
                direction = false;
                break;
            case ')' :
                cprime = '(';
                direction = true;
                break;
            default :
                return -1;
        }

        int count;

        // Go back or forward
        if (direction) {
            // Count is 1 initially because we have already `found' one closing bracket
            count = 1;

            // Get text[0,offset-1];
            String text = doc.substring(0, offset);

            // Scan backwards
            for (int i = offset - 1; i >= 0; i--) {
                // If text[i] == c, we have found another
                // closing bracket, therefore we will need
                // two opening brackets to complete the
                // match.
                char x = text.charAt(i);
                if (x == c)
                    count++;

                    // If text[i] == cprime, we have found a
                    // opening bracket, so we return i if
                    // --count == 0
                else if (x == cprime) {
                    if (--count == 0)
                        return i;
                }
            }
        }
        else {
            // Count is 1 initially because we have already `found' one opening bracket
            count = 1;

            // So we don't have to + 1 in every loop
            offset++;

            // Get text[offset+1,len];
            String text = doc.substring(offset, doc.length());

            // Scan forwards
            for (int i = 0; i < text.length(); i++) {
                // If text[i] == c, we have found another
                // opening bracket, therefore we will need
                // two closing brackets to complete the
                // match.
                char x = text.charAt(i);

                if (x == c)
                    count++;

                    // If text[i] == cprime, we have found an
                    // closing bracket, so we return i if
                    // --count == 0
                else if (x == cprime) {
                    if (--count == 0)
                        return i + offset;
                }
            }
        }

        // Nothing found
        return -1;
    }
}
