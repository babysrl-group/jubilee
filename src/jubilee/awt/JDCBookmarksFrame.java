package jubilee.awt;

import jubilee.toolkit.JBToolkit;

import javax.swing.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.Map;

/**
 * Displays all the bookmarks added by the user and allows them to double-click and locate the relevant annotation.
 *
 * @author Christos Christodoulopoulos
 */
public class JDCBookmarksFrame extends JFrame {
    private JList<String> bookmarksList;

	public JDCBookmarksFrame(final JBToolkit parent, Map<String, String> bookmarksText) {
		super("Bookmarks");
        DefaultListModel<String> bookmarks = new DefaultListModel<>();
        for (String bk : bookmarksText.keySet())
            bookmarks.addElement(bk + "\t" + bookmarksText.get(bk));
        this.bookmarksList = new JList<>(bookmarks);
        MouseAdapter doubleClickListener = new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    String indexStr = bookmarksList.getSelectedValue().split("\t")[0];
                    int index = Integer.parseInt(indexStr.split(":")[0]);
                    int predIndex = Integer.parseInt(indexStr.split(":")[1]);
                    parent.goToAnnotation(index, predIndex);

                } else super.mouseClicked(e);
            }
        };
        this.bookmarksList.addMouseListener(doubleClickListener);
		add(new JScrollPane(bookmarksList));
		
		setBounds(20, 20, 700, 700);
		setVisible(true);
	}

}
