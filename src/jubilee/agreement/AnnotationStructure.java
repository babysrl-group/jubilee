package jubilee.agreement;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * A simple indexer to store the values of the individual labels used by the annotators
 */
public class AnnotationStructure {
    private Map<String, Integer> labelToIndex = new HashMap<String, Integer>();
    private List<Integer> annotations1 = new ArrayList<Integer>();
    private List<Integer> annotations2 = new ArrayList<Integer>();

    public int encode(String label) {
        if (!labelToIndex.containsKey(label))
            labelToIndex.put(label, labelToIndex.size());
        return labelToIndex.get(label);
    }

    public int lexiconSize() {
        return labelToIndex.size();
    }

    public void add(String annotation1, String annotation2) {
        annotation1 = annotation1.substring(annotation1.lastIndexOf('-') + 1);
        annotation2 = annotation2.substring(annotation2.lastIndexOf('-') + 1);
        annotations1.add(encode(annotation1));
        annotations2.add(encode(annotation2));
    }

    public List<Integer> getAnnotations1() {
        return annotations1;
    }
    public List<Integer> getAnnotations2() {
        return annotations2;
    }
}
