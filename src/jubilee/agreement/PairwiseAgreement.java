package jubilee.agreement;

/**
 * @author Vivek Srikumar
 */
public abstract class PairwiseAgreement extends AnnotatorAgreement {
    public PairwiseAgreement(int numItems, int numLabels) {
        this(2, numItems, numLabels);
    }

    public PairwiseAgreement(int numAnnotators, int numItems, int numLabels) {
        super(numAnnotators, numItems, numLabels);
        if (numAnnotators != 2) {
            String className = this.getClass().toString();
            throw new IllegalArgumentException("Number of annotators is not two for an object of class " + className);
        }
    }

    @Override
    public double getObservedAgreement() {
        double Ao = 0;
        for (int i = 0; i < this.numItems; i++) {
            if (annotation[i][0] == annotation[i][1])
                Ao++;
        }

        Ao /= this.numItems;
        return Ao;
    }

    public void printAgreementMatrix() {
        int[][] matrix = new int[this.numLabels][this.numLabels];
        for (int i = 0; i < this.numItems; i++) {
            matrix[this.annotation[i][0]][this.annotation[i][1]]++;
        }

        for (int i = 0; i < numLabels; i++) {
            for (int j = 0; j < numLabels; j++) {
                System.out.print(matrix[i][j] + "\t");
            }
            System.out.println();
        }
    }
}